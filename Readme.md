# Install the WebAssembly Binary Toolkit

## From sources

https://github.com/WebAssembly/wabt

## Archlinux

https://archlinux.org/packages/community/x86_64/wabt/

```
pacman -Su wabt
```

# Build binary

```
make simple.wasm
```

# Read binary

## Explained

```
wat2wasm simple.wat -v
```

## Raw

```
xxd simple.wasm
```

# Docs

https://developer.mozilla.org/en-US/docs/WebAssembly/Text_format_to_wasm
https://developer.mozilla.org/en-US/docs/WebAssembly/Using_the_JavaScript_API
https://developer.mozilla.org/en-US/docs/WebAssembly/Rust_to_wasm
https://developer.mozilla.org/en-US/docs/WebAssembly/Understanding_the_text_format
